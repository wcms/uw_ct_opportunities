<?php

/**
 * @file
 * uw_ct_opportunities.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_opportunities_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_opportunities content'.
  $permissions['create uw_opportunities content'] = array(
    'name' => 'create uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_opportunities content'.
  $permissions['delete any uw_opportunities content'] = array(
    'name' => 'delete any uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_opportunities content'.
  $permissions['delete own uw_opportunities content'] = array(
    'name' => 'delete own uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_opportunities content'.
  $permissions['edit any uw_opportunities content'] = array(
    'name' => 'edit any uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_opportunities content'.
  $permissions['edit own uw_opportunities content'] = array(
    'name' => 'edit own uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
